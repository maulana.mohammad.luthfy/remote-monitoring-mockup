$("#login-button").click(function (event) {
  event.preventDefault();
  let logo = document.getElementById('logo')

  $('form').fadeOut(500);
  logo.classList.add('loading')

  setTimeout(() => {
    $('.wrapper').addClass('form-success');
    logo.classList.remove('loading')
    document.getElementById('screen-msg').innerHTML = 'Welcome'
  }, 3000)
});