var dummies = [{ "serial_no": "7058000174", "e_model": "Grand Am", "e_power": 1813, "e_speed": 2522, "konsumsi_bbm": 285, "is_deleted": true, "action": null },
{ "serial_no": "0321585593", "e_model": "Torino", "e_power": 1200, "e_speed": 2438, "konsumsi_bbm": 259, "is_deleted": true, "action": null },
{ "serial_no": "3359146409", "e_model": "Sorento", "e_power": 2831, "e_speed": 1948, "konsumsi_bbm": 226, "is_deleted": false, "action": null },
{ "serial_no": "6659960414", "e_model": "Jetta", "e_power": 2800, "e_speed": 1571, "konsumsi_bbm": 146, "is_deleted": false, "action": null },
{ "serial_no": "6206294706", "e_model": "3000GT", "e_power": 1635, "e_speed": 2720, "konsumsi_bbm": 107, "is_deleted": false, "action": null },
{ "serial_no": "4345687918", "e_model": "Golf III", "e_power": 1853, "e_speed": 1359, "konsumsi_bbm": 289, "is_deleted": false, "action": null },
{ "serial_no": "2596163357", "e_model": "E350", "e_power": 1814, "e_speed": 1869, "konsumsi_bbm": 130, "is_deleted": false, "action": null },
{ "serial_no": "4965891104", "e_model": "FR-S", "e_power": 1762, "e_speed": 1005, "konsumsi_bbm": 168, "is_deleted": false, "action": null },
{ "serial_no": "7229409470", "e_model": "Phaeton", "e_power": 2607, "e_speed": 2683, "konsumsi_bbm": 220, "is_deleted": true, "action": null },
{ "serial_no": "1806016362", "e_model": "Wrangler", "e_power": 1455, "e_speed": 1110, "konsumsi_bbm": 212, "is_deleted": true, "action": null },
{ "serial_no": "6996089034", "e_model": "Pacifica", "e_power": 1449, "e_speed": 2655, "konsumsi_bbm": 182, "is_deleted": true, "action": null },
{ "serial_no": "9397339575", "e_model": "Town Car", "e_power": 1298, "e_speed": 2577, "konsumsi_bbm": 196, "is_deleted": true, "action": null },
{ "serial_no": "2290324264", "e_model": "Equator", "e_power": 2797, "e_speed": 1636, "konsumsi_bbm": 145, "is_deleted": true, "action": null },
{ "serial_no": "4556517079", "e_model": "1500 Club Coupe", "e_power": 2953, "e_speed": 2186, "konsumsi_bbm": 155, "is_deleted": true, "action": null },
{ "serial_no": "1851315438", "e_model": "Firefly", "e_power": 2175, "e_speed": 2582, "konsumsi_bbm": 271, "is_deleted": true, "action": null },
{ "serial_no": "0044918003", "e_model": "L300", "e_power": 2301, "e_speed": 2883, "konsumsi_bbm": 212, "is_deleted": true, "action": null },
{ "serial_no": "3291400127", "e_model": "Trooper", "e_power": 2504, "e_speed": 2957, "konsumsi_bbm": 152, "is_deleted": true, "action": null },
{ "serial_no": "1236610601", "e_model": "G-Class", "e_power": 1434, "e_speed": 2842, "konsumsi_bbm": 120, "is_deleted": false, "action": null },
{ "serial_no": "4514800589", "e_model": "Vibe", "e_power": 2580, "e_speed": 1819, "konsumsi_bbm": 212, "is_deleted": false, "action": null },
{ "serial_no": "5339744965", "e_model": "Supra", "e_power": 1300, "e_speed": 1442, "konsumsi_bbm": 189, "is_deleted": false, "action": null }]

$(document).ready(() => {
  $('table').DataTable(
    {
      data: dummies,
      columns: [
        { data: 'serial_no' },
        { data: 'e_model' },
        { data: 'e_power' },
        { data: 'e_speed' },
        { data: 'konsumsi_bbm' },
        { data: 'is_deleted' },
        {
          data: null,
          searchable: false,
          bSortable: false,
          render: () => {
            return `
              <a href="javascript:void(0)">
                <em title="Edit" class="typcn typcn-pencil"></em>
              </a>`
          }
        }
      ]
    }
  )
})