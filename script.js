$(document).ready(() => {
  $('#menu-toggle').on('click', (e) => {
    e.stopPropagation();
    $('#menu-toggle-list').fadeToggle()
  })

  $(document).on('click', () => {
    $('#menu-toggle-list').hide()
  })
})